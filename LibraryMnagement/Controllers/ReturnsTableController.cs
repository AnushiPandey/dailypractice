﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using LibraryMnagement.Models;
using System.Data.Entity.SqlServer;

namespace LibraryMnagement.Controllers
{ [Authorize]
    public class ReturnsTableController : Controller
    {
        // GET: ReturnsTable
        private OnlineLibraryEntities2 db;
        public ReturnsTableController()
        {
            db = new OnlineLibraryEntities2();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Getmid(int id)
        {

            

                var m = (from s in db.Members where s.Id == id select s.Name).ToList();
                return Json(m, JsonRequestBehavior.AllowGet);
            }
        [HttpGet]
        public ActionResult GetReturn(int id)
        {
            var m = (from s in db.IssueBooks where s.MemberId == id select new
             { IssueDate=s.IDate,
               ReturnDate=s.RDate,
               BookName=s.BookId,
               Extra=SqlFunctions.DateDiff("day",s.RDate,DateTime.Now)
            }).ToArray();
            return Json(m,JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Save(ReturnsTable returnBook)
        {
            if (ModelState.IsValid)
            {
                db.ReturnsTables.Add(returnBook);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(returnBook);

        }
    }
    }
