﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web.Mvc.Ajax;
using Newtonsoft.Json;
using System.Net;

using LibraryMnagement.Models;

namespace LibraryMnagement.Controllers
{ [Authorize]
    public class IssueBookController : Controller
    {
        private OnlineLibraryEntities2 db = new OnlineLibraryEntities2();
        // GET: IssueBook
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult GetBooks()
        {
            var bookTables = db.BookTables.Select(s => new
            {
                Id=s.Id,
                Name=s.Bname

            }).ToList();
             return Json(bookTables,JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Getmid(int id)
        {
           
            var m = (from s in db.Members where s.Id == id select s.Name).ToList();
            return Json(m, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Save(IssueBook issueBook)
        {    if (ModelState.IsValid)
            {
                db.IssueBooks.Add(issueBook);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(issueBook);

        }
    }
}