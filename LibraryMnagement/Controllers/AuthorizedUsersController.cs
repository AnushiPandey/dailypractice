﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LibraryMnagement.Models;

namespace LibraryMnagement.Controllers
{
    public class AuthorizedUsersController : Controller
    {
      
        private OnlineLibraryEntities2 db;
        public AuthorizedUsersController()
        {
            db = new OnlineLibraryEntities2();
        }

        // GET: AuthorizedUsers
        public ActionResult Index()
        {
            return View(db.AuthorizedUsers.ToList());
        }

        // GET: AuthorizedUsers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuthorizedUser authorizedUser = db.AuthorizedUsers.Find(id);
            if (authorizedUser == null)
            {
                return HttpNotFound();
            }
            return View(authorizedUser);
        }

        // GET: AuthorizedUsers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AuthorizedUsers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UniqueId,Password,ConfirmPassword")] AuthorizedUser authorizedUser)
        {
            if (ModelState.IsValid)
            {
                db.AuthorizedUsers.Add(authorizedUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(authorizedUser);
        }

        // GET: AuthorizedUsers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuthorizedUser authorizedUser = db.AuthorizedUsers.Find(id);
            if (authorizedUser == null)
            {
                return HttpNotFound();
            }
            return View(authorizedUser);
        }

        // POST: AuthorizedUsers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UniqueId,Password,ConfirmPassword,M_Id")] AuthorizedUser authorizedUser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(authorizedUser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(authorizedUser);
        }

        // GET: AuthorizedUsers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuthorizedUser authorizedUser = db.AuthorizedUsers.Find(id);
            if (authorizedUser == null)
            {
                return HttpNotFound();
            }
            return View(authorizedUser);
        }

        // POST: AuthorizedUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AuthorizedUser authorizedUser = db.AuthorizedUsers.Find(id);
            db.AuthorizedUsers.Remove(authorizedUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
