﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LibraryMnagement.Models;

namespace LibraryMnagement.Controllers
{[Authorize]
    public class BookTablesController : Controller
    {
       
        private OnlineLibraryEntities2 db;
        public BookTablesController()
        {
            db = new OnlineLibraryEntities2();
        }

        // GET: BookTables
        public ActionResult Index()
        {
            var bookTables = db.BookTables.Include(b => b.Author).Include(b => b.BookCategory).Include(b => b.Publisher);
            return View(bookTables.ToList());
        }

        // GET: BookTables/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookTable bookTable = db.BookTables.Find(id);
            if (bookTable == null)
            {
                return HttpNotFound();
            }
            return View(bookTable);
        }

        // GET: BookTables/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.a_id = new SelectList(db.Authors, "Id", "Name");
            ViewBag.c_id = new SelectList(db.BookCategories, "Id", "Cname");
            ViewBag.p_id = new SelectList(db.Publishers, "Id", "Name");
            return View();
        }

        // POST: BookTables/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "Id,Bname,c_id,a_id,p_id,Contents,pages,edition")] BookTable bookTable)
        {
            if (ModelState.IsValid)
            {
                db.BookTables.Add(bookTable);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.a_id = new SelectList(db.Authors, "Id", "Name", bookTable.a_id);
            ViewBag.c_id = new SelectList(db.BookCategories, "Id", "Cname", bookTable.c_id);
            ViewBag.p_id = new SelectList(db.Publishers, "Id", "Name", bookTable.p_id);
            return View(bookTable);
        }

        // GET: BookTables/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookTable bookTable = db.BookTables.Find(id);
            if (bookTable == null)
            {
                return HttpNotFound();
            }
            ViewBag.a_id = new SelectList(db.Authors, "Id", "Name", bookTable.a_id);
            ViewBag.c_id = new SelectList(db.BookCategories, "Id", "Cname", bookTable.c_id);
            ViewBag.p_id = new SelectList(db.Publishers, "Id", "Name", bookTable.p_id);
            return View(bookTable);
        }

        // POST: BookTables/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "Id,Bname,c_id,a_id,p_id,Contents,pages,edition")] BookTable bookTable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bookTable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.a_id = new SelectList(db.Authors, "Id", "Name", bookTable.a_id);
            ViewBag.c_id = new SelectList(db.BookCategories, "Id", "Cname", bookTable.c_id);
            ViewBag.p_id = new SelectList(db.Publishers, "Id", "Name", bookTable.p_id);
            return View(bookTable);
        }

        // GET: BookTables/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookTable bookTable = db.BookTables.Find(id);
            if (bookTable == null)
            {
                return HttpNotFound();
            }
            return View(bookTable);
        }

        // POST: BookTables/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            BookTable bookTable = db.BookTables.Find(id);
            db.BookTables.Remove(bookTable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
