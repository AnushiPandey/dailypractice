﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
//using FormAuthentication.Models;
using LibraryMnagement.Models;



namespace LibraryMnagement.Controllers
{
    public class AccountController : Controller
    {
      
         
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(Models.User user)
        {
            using (var context = new Models.OnlineLibraryEntities2())
            {
                bool isValid = context.AuthorizedUsers.Any(x => x.UniqueId == user.UniqueId && x.Password == user.Password);
                ViewBag.Title = isValid;
                var user2 = context.AuthorizedUsers.Where(x => x.UniqueId == user.UniqueId && x.Password == user.Password).FirstOrDefault();
                if (isValid)
                {
                    FormsAuthentication.SetAuthCookie(user.UniqueId, false);
                    if (user2.UserRoles.Where(x=>x.Role.ToLower().Trim()=="admin").Count()>0)
                    {

                        return RedirectToAction("AdminDisplay");
                    }
                    else
                     {
                        return RedirectToAction("UserDisplay");

                    }
                }
                else
                {
                    ModelState.AddModelError("", "Invalid Username or Password");
                    return View();
                }
            }
           ;
        }
        public ActionResult Display()
        {
            return View();
        }
        public ActionResult UserDisplay()
        {
            return View();
        }
        public ActionResult LogOut()
        {
            
                FormsAuthentication.SignOut();
                return RedirectToAction("Login");
            

        }
        [Authorize(Roles = "Admin")]
        public ActionResult SignUp()
        {
            return View();
        }
        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult SignUp(Models.AuthorizedUser user)
        {
            using (var context = new Models.OnlineLibraryEntities2())
            {
               context.AuthorizedUsers.Add(user);
                context.SaveChanges();

            }
            return RedirectToAction("Index","AuthorizedUsers");
        }
        public ActionResult About()
        {
            return View();
        }
        public ActionResult AdminDisplay()
        {
            return View();
        }
    }
}