﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LibraryMnagement.Models
{
    [MetadataType(typeof(AuthorMetadata))]
    public partial class Author
    {
        public class AuthorMetadata
        {
            public int Id { get; set; }
            [DisplayName("Author's Name")]
            public string Name { get; set; }

            public string Address { get; set; }
            [DataType(DataType.PhoneNumber)]
            [Display(Name = "Contact Number")]
            [Required(ErrorMessage = "Phone Number Required!")]
            [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$",
                 ErrorMessage = "Entered phone format is not valid.")]
            public string Phone { get; set; }
        }
    }
}