﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace LibraryMnagement.Models
{
    public  partial class ReturnsTables
    {
        [MetadataType(typeof(ReturnsTableMetadata))]
        public  class ReturnsTableMetadata
        {
            public int Id { get; set; }
            public Nullable<int> Mid { get; set; }
            public string Book { get; set; }
            public Nullable<int> ExtraDays { get; set; }
            public Nullable<int> Fine { get; set; }
            public Nullable<System.DateTime> ReturnDate { get; set; }

           
        }
    }
}