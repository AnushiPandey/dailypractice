﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;



namespace LibraryMnagement.Models
{   [MetadataType(typeof(PublisherMetadata))]
    public  partial class Publisher
    {
        public class PublisherMetadata
        {
            public int Id { get; set; }
            [DisplayName("Publisher Name")]
            public string Name { get; set; }
            
            [DataType(DataType.PhoneNumber)]
            [Display(Name = "Contact Number")]
            [Required(ErrorMessage = "Phone Number Required!")]
            [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$",
                   ErrorMessage = "Entered phone format is not valid.")]
            public string Contactnum { get; set; }
            [DisplayName("Address"), StringLength(20)]
            public string Address { get; set; }
        }
    }
}