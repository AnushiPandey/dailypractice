﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LibraryMnagement.Models
{
    public class User
    { 
    
        public int Id { get; set; }
       
        public string UniqueId { get; set; }
        public string Password { get; set; }
        [Compare("Password",ErrorMessage ="Password should be same")]
        public string ConfirmPassword { get; set; }

    }

}