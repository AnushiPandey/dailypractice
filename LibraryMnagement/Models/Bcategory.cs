﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace LibraryMnagement.Models
{
    [MetadataType(typeof(BookcategoryMetadata))]
    public  partial class Bookcategory
    {  
        public class BookcategoryMetadata
        {
            public int Id { get; set; }
            [DisplayName("Category Name")]
            
            public string Cname { get; set; }
            [Required]
            [DisplayName("Book Status")]
            public string Status { get; set; }

        }
    }
}