﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;


namespace LibraryMnagement.Models
{
    
    public partial class IssueBook
    {
        public partial class IssueBookMetadata
        {
            public int Id { get; set; }
            public Nullable<int> MemberId { get; set; }
            public Nullable<int> BookId { get; set; }
            [DisplayName("Issue Date")]
            [DataType(DataType.DateTime)]
            [DisplayFormat(DataFormatString ="{0:dd/mm/yyyy}")]
           
            public Nullable<System.DateTime> IDate { get; set; }
            [DisplayName("Return Date")]
            [DataType(DataType.DateTime)]
            [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy}")]
            public Nullable<System.DateTime> RDate { get; set; }
        }
    }
}