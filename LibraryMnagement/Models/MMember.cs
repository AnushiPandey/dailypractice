﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace LibraryMnagement.Models
{
    [MetadataType(typeof(MemberMetadata))]
    public partial class Member
    {
        public class MemberMetadata
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Course { get; set; }
            public string Contactnum { get; set; }
            public string Address{ get; set; }
        }
    }
}