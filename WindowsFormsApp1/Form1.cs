﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form //Current class is Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Button b1 = new Button();
            b1.Text = "Click me"; //text on button
            b1.Size = new Size(50, 80); //button size
            b1.Location = new Point(100, 100); //location of button
            this.Controls.Add(b1);// Current class id Form now so this points to Form object
            b1.Click += new EventHandler(b1Message);//this is DElegate, where we are passing the methods
        }
        private void b1Message(object sender, EventArgs e)// Every method should have be defined using these parameters
        {
            MessageBox.Show("Hello World");
        }
    }
}
