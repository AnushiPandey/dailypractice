﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Array1
    {
        public void NewArray()
        {
            Console.WriteLine("Enter the row");
            int A = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter the Column");
            int B = Int32.Parse(Console.ReadLine());
            int[,] matrix1 = new int[A, B];

            for (int i = 0; i < A; i++)
            {
                for (int j = 0; j < B; j++)
                {
                    Console.WriteLine("Enter the element");
                    matrix1[i, j] = Int32.Parse(Console.ReadLine());
                }
            }

            for (int i = 0; i < A; i++)
            {
                for (int j = 0; j < B; j++)
                {
                    Console.WriteLine("Element({0},{1})={2}", i, j, matrix1[i, j]);
                }

            }

        }
                public static int Add(params int[] numbers)
            {
                var sum = 0;
                foreach (var number in numbers)
                {
                    sum += number;
                }

                return sum;

            }
    }
}
