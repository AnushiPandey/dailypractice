﻿using System;

namespace ConsoleApp1
{
    enum days
    {
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6,
        Sunday = 7
    }
    enum Months
    {
        January = 31,
        February = 28,
        March = 31,
        April = 30,
        May = 31,
        June = 30,
        July = 31,
        August = 31,
        September = 30,
        October = 31,
        November = 30,
        December = 31
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter 1 to perform Array operation");
            Console.WriteLine("Enetr 2 to perform Enum");
            Console.WriteLine("Enter 3 to pass value");
            Console.WriteLine("Enter 4 to perform String Operations");
            Console.WriteLine("Enter 5 to See use of Params");
            Console.WriteLine("Enter 6 for Overloading");
            int choice = Convert.ToInt32(Console.ReadLine());
            NewMethod(choice);
        }

        private static void NewMethod(int choice)
        {
            switch (choice)
            {
                case 1:
                    {
                        Array1 ob1 = new Array1();
                        ob1.NewArray();
                        break;


                    }
                case 2:
                    {


                        Console.WriteLine("what day Num you want to know");
                        string s1 = Convert.ToString(Console.ReadLine());
                        var New1 = Enum.Parse(typeof(days), s1);
                        Console.WriteLine("Which Month of the year you want to know ");
                        string s2 = Convert.ToString(Console.ReadLine());
                        var New2 = Enum.Parse(typeof(Months), s1);
                        Console.WriteLine();
                        Console.WriteLine("This is" + (New1) + " of the week");


                        Console.WriteLine();
                        Console.WriteLine("This is " + (New2) + "Month");


                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Enter value for passing using ref ");
                        int i = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Previous value of integer i:" + i.ToString());
                        string test = Passingvalues.GetNextName(ref i);
                        Console.WriteLine("Current value of integer i:" + i.ToString());
                        break;
                    }
                case 4:
                    {
                        StringOperation.Upper();
                        break;
                    }
                case 5:
                    {
                        Console.WriteLine("Default array is {1,2,3,4}");
                        Array1.Add(1, 2, 3, 4, 5);
                        break;
                    }
                case 6:
                    {
                        try
                        {
                            var point = new PointOverloading(10, 20); // initiallly it will be assigned to x and y
                            point.Move(new PointOverloading(60, 100));// point type argument is passed here
                            Console.WriteLine("Point is at ({0}, {1})", point.X, point.Y);
                            point.Move(100, 200);
                            Console.WriteLine("Point is at ({0}, {1})", point.X, point.Y);
                            point.Move(null);// for the exception

                        }
                        catch (Exception)
                        {
                            Console.WriteLine("An unexpected error occured.");

                        }
                        break;




                    }
                case 7 :
                    {
                        Console.WriteLine("Enter 2 to perform operation on two numbers /n " +
                        "Enter 1 to perform operation on 1 number /n" +
                        " Enter 3 to perform operation on singlr integer");
                        int Response = Convert.ToInt32(Console.ReadLine());
                        switch (Response)
                        {
                            case 1:
                                {
                                    float OnlyNumber = Convert.ToInt32(Console.ReadLine());
                                    MathOperations.Operations(OnlyNumber);
                                    break;
                                }
                            case 2:
                                {

                                    int Number1 = Convert.ToInt32(Console.ReadLine());
                                    int Number2 = Convert.ToInt32(Console.ReadLine());
                                    MathOperations.Operations(Number1, Number2);
                                    break;
                                }
                            case 3:
                                {
                                    int Sqrtnumber = Convert.ToInt32(Console.ReadLine());
                                    MathOperations.Operations(Sqrtnumber);
                                    break;
                                }
                                
                        }
                        break;
                    }

            }
        }
    }
    }
