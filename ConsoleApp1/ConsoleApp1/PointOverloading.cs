﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class PointOverloading
    {
        
            public int X;
            public int Y;

            public PointOverloading(int x, int y) //Constructor
            {
                this.X = x;
                this.Y = y;
            }

            public void Move(int x, int y)
            {
                this.X = x;
                this.Y = y;
            }

            public void Move(PointOverloading newLocation) // newlocation is an object passed of Point type
            {
                if (newLocation == null) //  if the object is null it will raise exception
                    throw new ArgumentNullException("newLocation");

                Move(newLocation.X, newLocation.Y);
            }
        }
    }

