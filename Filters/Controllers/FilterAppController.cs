﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using Filters.ActionFilters;// Mandatory

namespace Filters.Controllers
{
   // [OutputCache(Duration = 10)]// for all the action methods

    public class FilterAppController : Controller
    {  /*
        // GET: FilterApp
        
        //[OutputCache (Duration=10)]
        [OutputCache (Duration=10,Location =System.Web.UI.OutputCacheLocation.Server)]// if request is coming from several browsers
        public ActionResult Index()
        {
            return View();
        }
        */
        [LogActionFilter] // you can apply the Log action filter to an entire Controller  class
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }

    }
}