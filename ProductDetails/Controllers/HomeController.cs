﻿using ProductDetails.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.SqlClient;

namespace ProductDetails.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            DataContext db = new DataContext();
            var data = db.Products.SqlQuery("Select * from Products").ToList();
            return View(data);
        }
       

        // GET: Home/Details/5
        public ActionResult Details(int id)

        {

            DataContext db = new DataContext();
            var data = db.Products.SqlQuery("Select * from Products where ProductId=@p0",id).SingleOrDefault();
            return View(data);
           
        }

        // GET: Home/Create
        public ActionResult Create() 
        {

            return View();
        }

        // POST: Home/Create
        [HttpPost]
        public ActionResult Create(Products collection)
        {
           
                  DataContext db = new DataContext();
                 List<object> lst = new List<object>();
                 lst.Add(collection.ProductName);
                 lst.Add(collection.Company);
                 lst.Add(collection.SerialNumber);
                 object[] arr = lst.ToArray();
                 int d1 = db.Database.ExecuteSqlCommand("Insert into Products(Productname,company,SerialNumber) Values (@p0,@p1,@p2)", arr); 
                 if (d1 > 0)
                 {
                     ViewBag.Txt = "product added successfully";


                 
 
               

                //return RedirectToAction("Index");
                    }

            return View();


        }

        // GET: Home/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Home/Edit/5
        [HttpPost]
        public ActionResult Edit( Products collection)
        {
            try
            {
                DataContext obj = new DataContext();
                
                List<object> parameters = new List<object>();
               
                parameters.Add(collection.ProductName);
                parameters.Add(collection.SerialNumber);
                parameters.Add(collection.Company);
                parameters.Add(collection.ProductId);
                object[] objectarray = parameters.ToArray();
                
                int output = obj.Database.ExecuteSqlCommand("update Products set ProductName = @p0, SerialNumber = @p1, Company = @p2 where ProductId = @p3", objectarray);
                if (output > 0)
                {
                    ViewBag.Itemmsg = "Your Product " + collection.ProductName + "  is added Successfully";
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Home/Delete/5
        public ActionResult Delete(int id)
        {
            DataContext db = new DataContext();
            var productlist = db.Products.SqlQuery("select *from products").ToList();
            

            return View(productlist);
        
        }

        // POST: Home/Delete/5
        [HttpPost]
        public ActionResult Delete( Products collection) 
        {
            try
            {
                DataContext db = new DataContext();
                object id = collection.ProductId;
                var productlist = db.Database.ExecuteSqlCommand("delete from Products where ProductId=@p0",id );
                if (productlist != 0)
                {
                    //We will go back to action ProductDelete to show updated records
                    return View(productlist);
                }
                return View(productlist);
            }

            catch
            {
                return View();
            }
        }
    }
}
