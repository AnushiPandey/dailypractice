﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SessionApplication.Controllers
{
    public class SessionController : Controller
    {
        // GET: Session
        public ActionResult Index()
        {
            ViewBag.Data = "This is from Index";
            ViewData["var1"] = "View data from Index";
            Session["varchar2"] = "This is session data";
            return View();
        }
        public ActionResult About()
        {
            //return View();
            //return Content("Hello World");
            //return HttpNotFound();
            return RedirectToAction("Contact","SessionApplication");//specify the action and controller
        }
        public ActionResult Contact()
        { if (Session["varchar2"]!=null)
            {
                Session["varchar2"].ToString();
            }

            return View();

        }
    }
    }
