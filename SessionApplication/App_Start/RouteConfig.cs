﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SessionApplication
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                "MoviesByDate",
                "Movies/Release/{id}/{year}",
                 new { controller = "Movies", action = "Release", id = UrlParameter.Optional }// Convention based routing
                 new { id = @"/d{4}" }// we can also add constraints here
          );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Session", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
