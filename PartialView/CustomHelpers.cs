﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace PartialView
{
    public static class CustomHelpers
    {
        public static IHtmlString Images(string src,string alt)
        {
            return new MvcHtmlString(string.Format("<img src='{0}'>alt {1}</img>",src,alt));
        }
    }
}