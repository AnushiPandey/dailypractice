﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace PartialView.Models

{
    public class Customer
    {
        public string Name{ get; set; }
        [DataType(DataType.Date)]
        public  DateTime BuyTime { get; set; }
    }
}