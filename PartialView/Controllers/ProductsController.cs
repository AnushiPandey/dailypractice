﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PartialView.Models;

namespace PartialView.Controllers
{
    public class ProductsController : Controller
    {
        // GET: Products
        List<Product> products = new List<Product>()
        {
            new Product()
        {
            Name="OPPO",
            Image="~/Images/Oppo.jpg",
            Price=30000
        },
            new Product()
        {
            Name="Samsung",
            Image="~/Images/Samsung.jpg",
            Price=50000
        },
             new Product()
        {
            Name="Iphone",
            Image="~/Images/Iphone.jpg",
            Price=50000
        }





        };
        Customer c1= new Customer()
        {
            Name = "Anushi",
            BuyTime=DateTime.Today


        };
        public PartialViewResult Prototype()
        {
            return PartialView();
        }
        public ActionResult Index ()
        {
            return View(products);
        }
        public ActionResult Editor()
        {
            return View(c1);
        }
    }
}