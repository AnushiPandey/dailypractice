﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace CodeFirstConvention.Models
{
    public class Department
    {
        public int DepartmentId { get; set; }
        public string Dname { get; set; }
        //Navigation Property
        public ICollection<User> Users { get; set; } //Collection Navigation Property as 1 employee can have many departments
      

    }
}