﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodeFirstConvention.Models
{
    public class User
    {
        public int id { get; set; }

        public string Name { get;
            set;

        }
        public ICollection<Department> Departments { get; set; } //Navigation Property
    }
}