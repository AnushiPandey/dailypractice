﻿using CodeFirstConvention.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.Data.SqlClient;



namespace CodeFirstConvention.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        
       
        public ActionResult Index()
        {
            DataContext db = new DataContext();
            
            var data = db.Users.ToList();
            return View(data);
        }
    }
}