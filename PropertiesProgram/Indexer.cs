﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PropertiesProgram
{
    public class Indexer
    {
        private Dictionary<string, string> _dictionary;

        public Indexer()
        {
            _dictionary = new Dictionary<string, string>();// initializing the dictionary

        } 
        public string this[string key]
        {   get
            {
                return _dictionary[key];
            }
            set
            {
                _dictionary[key] = value;    
            }

        }
    }
}
