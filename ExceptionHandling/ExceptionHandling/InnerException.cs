﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ExceptionHandling
{
    class InnerException
    { public static void Inner()
        { StreamReader strRd = null;
            try
            {
                try

                {  // creating instance of StreamReader to read the files in File
                    strRd = new StreamReader(@"C:\Users\daffodil\Documents\New folder\File.txt");
                    int a = int.Parse(Console.ReadLine());
                    int b = int.Parse(Console.ReadLine());
                    int c = a / b;


                }
                // catching exception object
                catch (Exception ex)

                {
                    string StrPath = @"C: \Users\daffodil\Documents\New folder\File1.txt ";
                    if (File.Exists(StrPath))
                        {
                        strRd = new StreamReader(@"C:\Users\daffodil\Documents\New folder\File.txt");

                        Console.WriteLine(ex.Message);
                        Console.WriteLine("");
                        Console.WriteLine(ex.StackTrace);
                        }
                    else
                    {
                        throw new FileNotFoundException(StrPath+ "Not Present", ex); //constructor for current Exception: To  retain original exception pass it to a parameter to the constructor of current exception
                        //it will throw new exception created by us.
                       
                    }
                }
            }
            catch (Exception Ex1)// new object created for base class Exception
            {

                Console.WriteLine("Finally inner exception={0} ",Ex1.InnerException.GetType().Name);// it will print the inner exception
            }


            }
    }

}
