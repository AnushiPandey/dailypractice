﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;// to add all the models in the controller

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var data = GetMovie();
            return View(data);//passing the object
        }

        public ActionResult About()
        {
            ViewBag.Message = "Page is all about Movies";

            return View();//  Inside view we can pass the templates that are in shared folder example Error
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "My contact page.";

            return View();
        }
        public ActionResult Home()
        {
            ViewBag.Title = "Exploring New Movies everyday";
            ViewBag.Message = "Today's Special Horror";
            return View();
        }
        private Movie GetMovie()
        {
            return new Movie()
            {
                ID = 1,
                Title = "Harry Potter",

                Genre = "Horror",
                Price = 2000
            };
        }
    }
}
    
 
