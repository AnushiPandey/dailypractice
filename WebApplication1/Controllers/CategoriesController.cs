﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class CategoriesController : Controller
    {
        // GET: Categories
        /*public ActionResult Index() // Default Methods
        {
            return View();
        }
        */
        // GET: /HelloWorld/ 
        /*public string Index()
        {
            return "This is my <b>default</b> action to know basics about controller...";
        }*/

        // 
        // GET: /HelloWorld/Welcome/ 


        //return "This is the Welcome action method...";
        public ActionResult Index()
        {
            return View(); //Calling Controllers View method(In views we have Index.cshtml file)
        }
        public ViewResult Welcome(string name, int numTimes = 1)
        {
            ViewBag.Message = "Hello " + name;
            ViewBag.NumTimes = numTimes;

            return View();
        }

}
}