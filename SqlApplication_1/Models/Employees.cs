﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using System.ComponentModel.DataAnnotations;
namespace SqlApplication_1.Models
{
    public class Employees
    {
        [Key]
        [ScaffoldColumn(false)]
        public int EmpId { get; set; }
        [Required(ErrorMessage = "Enter Name"), MaxLength(20)]

        public string Name { get; set; }
        [DataType(DataType.Date)]

        [Required(ErrorMessage = "Enter Birthdate")]
        public DateTime Birthdate { get; set; }
        [Required]

        public string Department { get; set; }

       
    }
}