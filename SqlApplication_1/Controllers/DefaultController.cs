﻿using SqlApplication_1.DataAccess;
using SqlApplication_1.Models;
using System;
using System.Web.Mvc;


namespace SqlApplication_1.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult InsertCustomer()

        {

            return View();

        }


        [HttpPost]
        public ActionResult InsertCustomer( Employees objEmp)

        {
           

             if (ModelState.IsValid) //checking model is valid or not

             {

                 DataAccessLayer obEmployee = new DataAccessLayer();

                 string result = obEmployee.InsertData(objEmp);

                 ViewData["result"] = result;

                 ModelState.Clear();

                 

             }

             else

             {

                 ModelState.AddModelError("", "Error in saving data");

                 return View();



             }
            return View(objEmp);

        }
    }
}

