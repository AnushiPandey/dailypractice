﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplicationWithJQuery.Models;
using Newtonsoft.Json;

namespace WebApplicationWithJQuery.Controllers
{
    public class HomeController : Controller //GetData from Ajax
    {
        // GET: Home
        public ActionResult Index()// for displaying through ajax in view
        {   
            return View();
        }
        public JsonResult GetStudent()
        {
            Student sd = new Student()
            {
                id = 1, Name = "Anushi", Subject = "English"            };
            var json = JsonConvert.SerializeObject(sd);//object paassed 
            return Json(json, JsonRequestBehavior.AllowGet);
        }
    }
}