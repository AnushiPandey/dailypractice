﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using WebApplicationWithJQuery.Models;

namespace WebApplicationWithJQuery.Controllers
{
    public class FormController : Controller
    {
        // GET: Form
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        public JsonResult AddStudent(Student student)//getting data from view using ajax
        {
            return Json(student, JsonRequestBehavior.AllowGet);
        }
        
    }
}