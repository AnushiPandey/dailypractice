﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using WebApplicationWithJQuery.Models;

namespace WebApplicationWithJQuery.Controllers
{
    public class BeginFormController :Controller
    {
        // GET: BeginForm
        public ActionResult Indexnew()
        {
            return View();
        }
        [System.Web.Mvc.HttpPost]
        public ActionResult IndexnewTest(Student model)
        {
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
/*public class IssueBookController : Controller
{
    private OnlineLibraryEntities db = new OnlineLibraryEntities();
    // GET: IssueBook
    public ActionResult Index()
    {
        return View();
    }
    [HttpPost]
    public ActionResult Getmid(int id)
    {
        Member m1 = db.Members.Find(id);
        var m = (from s in db.Members where s.Id == id select s.Name).ToList();
        return Json(m, JsonRequestBehavior.AllowGet);
    }
        /*public ActionResult Save(IssueBook issueBook)
        {
            db.IssueBooks.Add(issueBook);
            db.SaveChanges();

        }*/