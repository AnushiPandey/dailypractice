﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationWithJQuery.Models
{
    public class Student
    {
        public int id { get; set; }
        public string Name { get; set; }
        public String Subject { get; set; }
    }
}