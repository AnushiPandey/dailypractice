﻿using System;
using System.IO;
using System.Collections;

namespace Casting
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            /* Uplifting
            
             Text text = new Text();
            Shape shape = text;// both are pointing to same object in memory
            shape.Width = 100;
            text.Width = 300;
            Console.WriteLine(shape.Width);// It will print 300 coz reference is same for text and shape
            StreamReader reader = new StreamReader(new FileStream(@"C:\Users\daffodil\Source\Repos", FileMode.Open));
            //Here we can pass any object which is derived by Strean class (ex; FileStream,MemorySream)
           */
            Shape shape = new Text();// Here we have only access to shape class methods only because Text is uplifted to Shape(Upcasting)
            Text text = (Text)shape;//DownCasting, Now we can use the methods of text using text object
            text.Font ="Drk" ;
           


        }
    }
}
