﻿using RepositoryPattern.Models;
using RepositoryPattern.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RepositoryPattern.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        private IStudentRepository _dataContext;// object of interface
        public HomeController()
        {
            this._dataContext = new StudentRepository(new StudentEntities1());// this object will work further

        }
        public ActionResult Index()
        {
            var data = (from m in _dataContext.GetAll()
                        select m);
            return View(data);
        }

        // GET: Home/Details/5
        public ActionResult Details(int id)
        {
            Table_3 t = _dataContext.GetStudent(id);
            return View(t);
        }

        // GET: Home/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Home/Create
        [HttpPost]
        public ActionResult Create(Table_3 obj)
        {
            try
            {
                _dataContext.InsertStu(obj);
                _dataContext.Save();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Home/Edit/5
        public ActionResult Edit(int id)
        {
            Table_3 t = _dataContext.GetStudent(id);
            return View(t);
            
        }

        // POST: Home/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Table_3 Obj)
        {
            try
            {
                _dataContext.Update(Obj);
                _dataContext.Save();

                return RedirectToAction("Index");

            }
            catch
            {
                return View();
            }
        }

        // GET: Home/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Home/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
