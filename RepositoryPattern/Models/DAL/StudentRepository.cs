﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RepositoryPattern.Models.DAL
{
    public class StudentRepository : IStudentRepository
    {
        private StudentEntities1 _dataContext;// object of datacontext
        public StudentRepository(StudentEntities1 context)
        {
            this._dataContext = context;// this object will work further

        }
        public void DeleteStu(int Id)
        {
            Table_3 table = _dataContext.Table_3.Find(Id);
            _dataContext.Table_3.Remove(table);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Table_3> GetAll()
        {
            return _dataContext.Table_3.ToList();
        }

        public Table_3 GetStudent(int Id)
        {
            return _dataContext.Table_3.Find(Id); ;
        }

        public void InsertStu(Table_3 table)
        {
            _dataContext.Table_3.Add(table);
        }

        public void Save()
        {
            _dataContext.SaveChanges();
        }

        public void Update(Table_3 table)
        {
            _dataContext.Entry(table).State = System.Data.Entity.EntityState.Modified;
        }
    }
}