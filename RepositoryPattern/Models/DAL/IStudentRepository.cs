﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryPattern.Models.DAL
{
    interface IStudentRepository:IDisposable
    {
        Table_3 GetStudent(int Id);
        void InsertStu(Table_3 table);
        void DeleteStu(int Id);
        void Update(Table_3 table);
        void Save();
        IEnumerable<Table_3> GetAll();
    }
}
