﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RoutingApplication.Models;

namespace RoutingApplication.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult GetAllStudent()
        {
            var student = students();
            return View(student);// strongly type binding
        }
        public ActionResult GetStudent(int id)
        {
            var student = students().FirstOrDefault(x => x.Id == id);// it will generate the first record
            return View(student);

        }
        public ActionResult GetAddress(int id)
        {
            var studentaddress = students().Where(x => x.Id == id).Select(x => x.Address);
            return View();

        }
        private List<Student> students()
        {
            return new List<Student>()
            {
                new Student()
                {
                    Id = 1,
                    Name = "Anushi",
                    Address = new Addres()
                    {
                        Housenum = 56,

                        City = "Lucknow"

                    }
                },
                new Student()
                {
                    Id = 1,
                    Name = "Anu",
                    Address = new Addres()
                    {
                        Housenum = 57,

                        City = "Lucknow"

                    }
                },
                new Student()
                {
                    Id = 1,
                    Name = "Ansh",
                    Address = new Addres()
                    {
                        Housenum = 58,

                        City = "Lucknow"

                    }

                },



            };
        }

    } }

    

    
