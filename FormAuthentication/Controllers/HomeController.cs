﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FormAuthentication.Models;
using System.Web.Security;
using Newtonsoft.Json;


namespace FormAuthentication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(Models.Membership model)
        {
            using (var context = new Models.OfficeEntities())
            {
                bool isValid = context.User.Any(x => x.Name == model.Name && x.Password == model.Password);
                if (isValid == true)
                {
                    FormsAuthentication.SetAuthCookie(model.Name, false);
                    return RedirectToAction("Index", "Employees");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid Username or password");
                    return View();
                }
            }
           ;
        }
        public ActionResult SignUp()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SignUp(User user)
        {
            using (var context = new OfficeEntities())
            {
                context.User.Add(user);
                context.SaveChanges();

            }
            return RedirectToAction("Login");
        }
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        public JsonResult Display()
            
        {
            var context = new OfficeEntities();
           
            var json = JsonConvert.SerializeObject(context.Employee.ToList());
            return Json (data: json, JsonRequestBehavior.AllowGet);
        }

    }
}