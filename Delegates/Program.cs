﻿using System;

namespace Delegates
{
    public delegate int NumberChanger(int n);// Delegate declaration
    public delegate string SayHello(string s); //Delegate declaration returntype string
    class Program
    {
       
            static int num = 10;
            public string st;

            public  int AddNum(int p)
            {
                num += p;
                return num;
            }
            public static int MultNum(int q)
            {
                num *= q;
                return num;
            }
            public static int getNum()
            {
            return num;
            }
            public string getString(string s)
        {
            st = s;
            return st ; 
            }
        static void Main(string[] args)
        {
            Program p = new Program();
            NumberChanger nc1 = new NumberChanger(p.AddNum);// Delegate Instantiation(calling Non-static method) 
            NumberChanger nc2 = new NumberChanger(MultNum);// Delegate Instantiation(calling static method)
            SayHello nc3 = new SayHello(p.getString);
            NumberChanger nc;
            nc = nc1;// Multicasting Delegate
            nc += nc2;// but the result that will be returned will be of last method
            Console.WriteLine("calling nc delegate;{0}",nc(5));

            //calling the methods using the delegate objects
           
            Console.WriteLine("Value of Num: {0}", nc1(10));
            
            Console.WriteLine("Value of Num: {0}", nc2(5));
            Console.WriteLine("Value of String:{0}",nc3("New"));
            Console.ReadKey();
            Console.WriteLine("Hello World!");
        }
    }

}
