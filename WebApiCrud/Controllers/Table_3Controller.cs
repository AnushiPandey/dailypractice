﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApiCrud.Models;

namespace WebApiCrud.Controllers
{
    public class Table_3Controller : ApiController
    {
        private StudentEntities db = new StudentEntities();

        // GET: api/Table_3
        public IQueryable<Table_3> GetTable_3()
        {
            return db.Table_3;
        }

        // GET: api/Table_3/5
        [ResponseType(typeof(Table_3))]
        public IHttpActionResult GetTable_3(int id)
        {
            Table_3 table_3 = db.Table_3.Find(id);
            if (table_3 == null)
            {
                return NotFound();
            }

            return Ok(table_3);
        }

        // PUT: api/Table_3/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTable_3(int id, Table_3 table_3)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != table_3.Id)
            {
                return BadRequest();
            }

            db.Entry(table_3).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Table_3Exists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Table_3
        [ResponseType(typeof(Table_3))]
        public IHttpActionResult PostTable_3([FromBody]Table_3 table_3)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Table_3.Add(table_3);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = table_3.Id }, table_3);
        }

        // DELETE: api/Table_3/5
        [ResponseType(typeof(Table_3))]
        public IHttpActionResult DeleteTable_3(int id)
        {
            Table_3 table_3 = db.Table_3.Find(id);
            if (table_3 == null)
            {
                return NotFound();
            }

            db.Table_3.Remove(table_3);
            db.SaveChanges();

            return Ok(table_3);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Table_3Exists(int id)
        {
            return db.Table_3.Count(e => e.Id == id) > 0;
        }
    }
}