﻿using System;

namespace DelegatesEvents
{
    

        public delegate void EventHandler(string a);

        public class Operation
        {
            public event EventHandler Event1;

            public void Action(string a)
            {
                if (Event1 != null)
                {
                    Event1(a);
                    Console.WriteLine(a);
                }
                else
                {
                    Console.WriteLine("Not Registered");
                }
            }
        }

        class Program
        {
            public static void CatchEvent(string s)
            {
                Console.WriteLine("Method Calling");
            }

            static void Main(string[] args)
            {
                Operation o = new Operation();

                o.Action("Event Calling");
                o.Event1 += new EventHandler(CatchEvent); //CatchEvent method that would be referenced in the delegate convocation list
                 o.Action("Event Calling"); //  Now event is not null when second time the Action is called


            Console.ReadLine();
            }
        }
    }
   
        
    


